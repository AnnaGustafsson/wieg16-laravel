<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->tinyInteger('gender')->nullable(); //unsignedTinyInteger
            $table->tinyInteger('customer_activated')->nullable(); //boolean
            $table->smallInteger('group_id')->nullable(); //unsignedBigInteger
            $table->unsignedBigInteger('company_id')->nullable(); //
            $table->string('customer_company')->nullable();
            $table->smallInteger('default_billing')->nullable(); //unsignedBigInteger
            $table->smallInteger('default_shipping')->nullable(); //unsignedBigInteger
            $table->tinyInteger('is_active')->nullable(); //boolean
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('customer_invoice_email')->nullable();
            $table->string('customer_extra_text')->nullable();
            $table->integer('customer_due_data_period')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
