<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('customers', 'CustomerController');

/*
Route::get('/customers', 'CustomersController@showCustomers');

Route::get('/customers/{id}', 'CustomersController@showCustomer');

Route::get('/customers/{id}/address', 'CustomersController@showCustomerAddress');
*/
Route::get('/companies', 'CustomersController@showCompanies');

Route::get('/customers/by-company/{id}', 'CustomersController@showCustomersByCompany');
Route::get('/createproduct', 'ProductController@store');



Route::get('/fb-login', 'FacebookController@index');
Route::get('/login', 'FacebookController@loginForm');
Route::get('/facebook', 'FacebookController@facebook');


Route::get('/stripe', 'StripeController@index');
Route::get('/instagram', 'InstagramImagesController@index');
Route::get('/tweets', 'TweetController@index');
Route::get('/words', 'TweetController@countWords');
Route::get('/stopwords', 'TweetController@excludeWords');
Route::get('/searchtweet', 'TweetController@searchTweet');

Route::get('/klarna-checkout', 'KlarnaController@index');
Route::get('/klarna-confirmation', 'KlarnaController@confirmation');
Route::get('/klarna-acknowledge', 'KlarnaController@acknowledge');
/*
Route::get('/example', '\App\Http\Controllers\Controller@example');

Route::get('customer/address', function () {
    return 'Customer ';
});

Route::get('/customers', function () {
    return view('customers');
});*/