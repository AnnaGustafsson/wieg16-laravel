<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    // Primary key-kolumnen antas vara auto-inkrementerande
    public $incrementing = false;

    // Laravel sköter timestamps åt dig om du inte säger nej
    public $timestamps = false;

    protected $fillable = [
        "id",
        "text",
    ];

    static public function count_words($tweets)
    {
        $words = [];
        foreach ($tweets as $tweet) {
            $words = array_merge($words, explode(" ", $tweet));
        }
        return (array_count_values($words));
    }

    static public function exclude_words($tweets)
    {
        $stopWords = array('och', 'vi', 'in');

        $words = [];
        foreach ($tweets as $tweet) {
            $words = array_merge($words, explode(" ", $tweet));
        }

        foreach ($words as $key => $value) {
            if (in_array($value, $stopWords)) {
                unset($words[$key]);
            }
        }
        return (array_count_values($words));
    }

    static function find_tweet($response)
    {
        $words = [];
        foreach ($response['statuses'] as $tweet) {
            $tweet = $tweet['text'];
            $words = array_merge($words, explode(" ", $tweet));
        }
        $sorted = array_count_values($words);
        asort($sorted);
        return array_reverse($sorted);
    }
}
