<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tweets', ['tweets' => Tweet::all()]);
    }

    public function countWords()
    {
        $tweets = Tweet::all('text');
        $result = Tweet::count_words($tweets);
        return response()->json($result);
    }


    public function excludeWords()
    {
        $tweets = Tweet::all();
        $result = Tweet::exclude_words($tweets);
        return response()->json($result);
    }


    public function searchTweet()
    {

        $searchWord = "";
        $tweet = [];
        if (isset($_GET["search"]))
        {
            $searchWord = $_GET['search'];
            $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q='.$searchWord.'",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer AAAAAAAAAAAAAAAAAAAAAM%2Br3QAAAAAAabkeHrA3wqb42iyNE8BVjsT686c%3DSWQn9PjagFuTLhziRGf3DkVHRTkqDh8RC8gMZTxDFwJquAgEHC",
                "cache-control: no-cache",
                "postman-token: c37a0dd4-6735-2161-0364-5cc41c44a8f8"
            ),
        ));


            $response = json_decode(curl_exec($curl), true);
            curl_close($curl);
            $tweet = Tweet::find_tweet($response);
        }
        return View('searchtweet', ['tweets' => $tweet]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
