<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Address;
use App\Company;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class CustomersController extends Controller
{

    //Customer hasOne Address, Address belongsTo Customer
    //return response()->json(Customer::with(['address'])->get());

    public function showCustomers()
    {
        return response()->json(Customer::all());
    }

    public function showCustomer($id)
    {
        $customer = Customer::Find($id);
        if ($customer == true) {
            return response()->json($customer);
        } else {
            return response()->json(["message" => "Customer not found"], 404);
        }

        //$response = Customer::find($id) ?? ['message'=> 'Customer not found'];
        //$statusCode = (is_object($response)) ? 200 :404;
        //return response()->json($response, $statusCode);
    }

    public function showCustomerAddress($id)
    {
        // Get all data from the specific row.
        //$address = Address::where('customer_id', $id)->get();
        // Get only street, postcode and city from the specific row.
        $address = Address::select('street', 'postcode', 'city')->where('customer_id', $id)->get();

        if (count($address) > 0) {
            return response()->json($address);
        } else {
            return response()->json(["message" => "Customer not found"], 404);
        }


        //showAddress
        //$response = Address::where('customer_id', '=', $id)->first() ?? ['message'=> 'Customer not found'];
        //$statusCode = (is_object($response)) ? 200 :404;
        //return response()->json($response, $statusCode);
    }


    public function showCompanies()
    {
        return response()->json(Customer::select('customer_company')->get());
    }

    public function showCustomersByCompany($id)
    {
        $company = Customer::where('company_id', $id)->get();
        if (count($company) > 0) {
            return response()->json($company);
        } else {
            return response()->json(["message" => "Customer not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
