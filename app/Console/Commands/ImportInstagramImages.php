<?php

namespace App\Console\Commands;

use App\InstagramImage;
use Illuminate\Console\Command;

class ImportInstagramImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:instagram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.instagram.com/v1/users/self/media/recent/?access_token=205549165.8d9bc2b.a363a8ac726e41b6bc4ff05eb035afe7",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 8756fc34-c551-5581-7905-4f8196dfa2a4"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);


        foreach ($response['data'] as $image) {
            $this->info("Inserting/updating image with id: " . $image['id']);
            $dbImage = InstagramImage::findOrNew($image['id']);
            $url = $image['images']['standard_resolution']['url'];
            //$caption = $image['caption']['text'];

            $dbImage->fill(['id' => $image['id'], 'url' => $url])->save();


        }
    }
}
