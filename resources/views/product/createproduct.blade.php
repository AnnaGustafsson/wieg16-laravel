<?php
/**
 * Created by PhpStorm.
 * User: annagustafsson
 * Date: 2017-12-05
 * Time: 10:49
 */

?>
<form action="<?= action('ProductController@store') ?>" method="post">
    <?= csrf_field() ?>
    <?= method_field('PUT')?>
        Entity id: <br> <input type="text" name="entity_id"><br>
        <br>
        SKU:<br> <input type="text" name="sku"><br>
        <br>
        Name:<br> <input type="text" name="name"><br>
        <br>
        Amount/package:<br> <input type="text" name="amount_package"><br>
        <br>
        Price:<br> <input type="text" name="price"><br>
        <br>
    <input type="submit" value="Submit">
</form>
